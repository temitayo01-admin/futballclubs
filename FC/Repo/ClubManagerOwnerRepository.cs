﻿using FC.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Repo
{
    public interface IClubManagerOwnerRepository
    {
        ReturnObject GetAll();
        ReturnObject GetAllValid();
        ReturnObject GetById(int Id);
        ReturnObject GetByClubId2(int cId);
        // List<ClubManagerOwnerModel> GetByClubId(int cId);
        ReturnObject DeleteById(int Id);
        ReturnObject DeleteByClubId(int Id);
        ReturnObject Add(ClubManagerOwnerModel model);
    }
    public class ClubManagerOwnerRepository : IClubManagerOwnerRepository
    {
        private TyDbContext _db;
        private string errMsg;
        public ClubManagerOwnerRepository(TyDbContext db)
        {
            _db = db;
        }
        public ReturnObject Add(ClubManagerOwnerModel model)
        {
            try
            {
                var reslt = DeleteByClubId(model.ClubId);
                if (!reslt.Status)
                {

                }
                //var reslt = _db.ClubManagerOwners.Where(c => c.ClubId == model.ClubId);
                //foreach (var i in reslt)
                //{
                //    i.IsDeleted = true;
                //    _db.ClubManagerOwners.Update(i);

                //}
                _db.Add(model);
                _db.SaveChanges();

                return new ReturnObject { StatusMessage = "Added successfully", Status = true };
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ReturnObject DeleteById(int Id)
        {
            try
            {
                var reslt = _db.ClubManagerOwners.Find(Id);
                if (reslt == null)
                {
                }
                reslt.IsDeleted = true;
                _db.ClubManagerOwners.Update(reslt);
                _db.SaveChanges();
                return new ReturnObject { StatusMessage = "Manager Deleted successfully", Status = true };
            }
            catch (Exception)
            {

                throw;
            }
            throw new NotImplementedException();
        } 
        public ReturnObject DeleteByClubId(int Id)
        {
            try
            {
                var reslt = _db.ClubManagerOwners.Where(c => c.ClubId == Id);
                foreach(var i in reslt)
                {
                    i.IsDeleted = true; 
                    _db.ClubManagerOwners.Update(i);
                    
                }
                _db.SaveChanges();
                return new ReturnObject { StatusMessage = "Manager Deleted successfully", Status = true };
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ReturnObject GetAll()
        {
            try
            {
                var ret = (from c in _db.ClubManagerOwners
                           join e in _db.Clubs on c.ClubId equals e.Id

                           select new
                           {
                               c.ClubId,
                               c.ManagerStartDay,
                               e.ClubName,
                               c.IsDeleted,
                               c.DateCreated,
                               c.PresentManager
                           }).ToList();
                if (ret.Count == 0)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return new ReturnObject { Status = false, StatusMessage = errMsg };
            }
        }

        public ReturnObject GetAllValid()
        {
            try
            {
                var ret = (from c in _db.ClubManagerOwners
                           join e in _db.Clubs on c.ClubId equals e.Id

                           select new
                           {
                               c.ClubId,
                               c.ManagerStartDay,
                               e.ClubName,
                               c.IsDeleted,
                               c.DateCreated,
                               c.PresentManager
                           }).Where(x => x.IsDeleted == false).ToList();
                if (ret.Count == 0)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return new ReturnObject { Status = false, StatusMessage = errMsg };
            }
        }

        //public List<ClubManagerOwnerModel> GetByClubId(int cId)
        //{
        //    try
        //    {
                //var ret1 = _db.ClubManagerOwners.ToList();
                //var ret1 = GetByClubId2(cId);
                //var ret2 = ret1.Select(c => new ClubManagerOwnerModel
                //{
                //    ClubId = c.ClubId,
                //    ManagerStartDay = c.ManagerStartDay,
                //    cl = e.ClubName,
                //    c.IsDeleted,
                //    c.DateCreated,
                //    c.PresentManager
                //}).ToList(); );
                //var ret = (from c in _db.ClubManagerOwners
                //           join e in _db.Clubs on c.ClubId equals e.Id
                //           where c.ClubId == cId
                //           select new
                //           {
                //               c.ClubId,
                //               c.ManagerStartDay,
                //               e.ClubName,
                //               c.IsDeleted,
                //               c.DateCreated,
                //               c.PresentManager
                //           }).ToList();
                //if (ret.Count == 0)
                //{
                //    return null;
                //}
               
            //    var rett= _db.ClubManagerOwners.Where(c => c.ClubId == cId)
            //               .Join(_db.Clubs.Where(e => e.ClubId == cId),
            //                e => e.Id,
            //                c => c.ClubId,
            //                (c, e) => new { ClubManagerOwners = c, Clubs = e }).ToList());

            //}
            //catch (Exception)
            //{
            //    return null;
            //}
       // }
        public ReturnObject GetByClubId2(int cId)
        {
            try
            {
                // var k = new List<ClubManagerOwnerModel>();
                var ret = (from c in _db.ClubManagerOwners
                           join e in _db.Clubs on c.ClubId equals e.Id
                           where c.ClubId == cId
                           select new
                           {
                               c.ClubId,
                               c.ManagerStartDay,
                               e.ClubName,
                               c.IsDeleted,
                               c.DateCreated,
                               c.PresentManager
                           }).ToList();
                if (ret.Count == 0)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ReturnObject GetById(int Id)
        {
            try
            {
                var ret = (from c in _db.ClubManagerOwners
                           join e in _db.Clubs on c.ClubId equals e.Id

                           select new
                           {
                               c.Id,
                               c.ClubId,
                               c.ManagerStartDay,
                               e.ClubName,
                               c.IsDeleted,
                               c.DateCreated,
                               c.PresentManager
                           }).Where(x => x.Id == Id);
                if (ret == null)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return new ReturnObject { Status = false, StatusMessage = errMsg };
            }
        }
    }
}
