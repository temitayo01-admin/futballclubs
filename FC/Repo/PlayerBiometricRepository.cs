﻿using FC.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Repo
{
    public interface IPlayerBiometricRepository 
    {
        ReturnObject AddAsync(PlayerBiometricModel model);
        IQueryable<PlayerBiometricModel> GetById(int Id);
    }
    public class PlayerBiometricRepository : IPlayerBiometricRepository
    {
        private TyDbContext _db;
        private string errMsg;
        public PlayerBiometricRepository(TyDbContext db)
        {
            _db = db;
        }
        public ReturnObject AddAsync(PlayerBiometricModel model)
        {
            try
            {
                _db.Add(model);
                _db.SaveChanges();

                return new ReturnObject { StatusMessage = "Added successfully", Status = true };
            }
            catch (Exception)
            {

                throw;
            }
        }

       
        public IQueryable<PlayerBiometricModel> GetById(int Id)
        {
            try
            {
                return from c in _db.PlayerBiometrics
                       where c.Id == Id
                       select c;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
