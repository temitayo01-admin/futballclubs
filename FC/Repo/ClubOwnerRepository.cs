﻿using FC.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Repo
{
    public interface IClubOwnerRepository
    {
        ReturnObject GetAll();
        ReturnObject GetAllValid();
        ReturnObject GetById(int Id);
        ReturnObject GetByClubId2(int cId);
        // List<ClubOwnerModel> GetByClubId(int cId);
        ReturnObject DeleteById(int Id);
        ReturnObject DeleteByClubId(int Id);
        ReturnObject Add(ClubOwnerModel model);
    }
    public class ClubOwnerRepository : IClubOwnerRepository
    {
        private TyDbContext _db;
        private string errMsg;
        public ClubOwnerRepository(TyDbContext db)
        {
            _db = db;
        }
        public ReturnObject Add(ClubOwnerModel model)
        {
            try
            {
                var reslt = DeleteByClubId(model.ClubId);
                if (!reslt.Status)
                {

                }
                //var reslt = _db.ClubOwners.Where(c => c.ClubId == model.ClubId);
                //foreach (var i in reslt)
                //{
                //    i.IsDeleted = true;
                //    _db.ClubOwners.Update(i);

                //}
                _db.Add(model);
                _db.SaveChanges();

                return new ReturnObject { StatusMessage = "Added successfully", Status = true };
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ReturnObject DeleteById(int Id)
        {
            try
            {
                var reslt = _db.ClubOwners.Find(Id);
                if (reslt == null)
                {
                }
                reslt.IsDeleted = true;
                _db.ClubOwners.Update(reslt);
                _db.SaveChanges();
                return new ReturnObject { StatusMessage = "Manager Deleted successfully", Status = true };
            }
            catch (Exception)
            {

                throw;
            }
            throw new NotImplementedException();
        }
        public ReturnObject DeleteByClubId(int Id)
        {
            try
            {
                var reslt = _db.ClubOwners.Where(c => c.ClubId == Id);
                foreach (var i in reslt)
                {
                    i.IsDeleted = true;
                    _db.ClubOwners.Update(i);

                }
                _db.SaveChanges();
                return new ReturnObject { StatusMessage = "Manager Deleted successfully", Status = true };
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ReturnObject GetAll()
        {
            try
            {
                var ret = (from c in _db.ClubOwners
                           join e in _db.Clubs on c.ClubId equals e.Id

                           select new
                           {
                               c.ClubId,
                               c.OwnershipStartDay,
                               e.ClubName,
                               c.IsDeleted,
                               c.DateCreated,
                               c.PresentOwner
                           }).ToList();
                if (ret.Count == 0)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return new ReturnObject { Status = false, StatusMessage = errMsg };
            }
        }

        public ReturnObject GetAllValid()
        {
            try
            {
                var ret = (from c in _db.ClubOwners
                           join e in _db.Clubs on c.ClubId equals e.Id

                           select new
                           {
                               c.ClubId,
                               c.OwnershipStartDay,
                               e.ClubName,
                               c.IsDeleted,
                               c.DateCreated,
                               c.PresentOwner
                           }).Where(x => x.IsDeleted == false).ToList();
                if (ret.Count == 0)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return new ReturnObject { Status = false, StatusMessage = errMsg };
            }
        }

        public ReturnObject GetByClubId2(int cId)
        {
            try
            {
                var ret = (from c in _db.ClubOwners
                           join e in _db.Clubs on c.ClubId equals e.Id
                           where c.ClubId == cId
                           select new
                           {
                               c.ClubId,
                               c.OwnershipStartDay,
                               e.ClubName,
                               c.IsDeleted,
                               c.DateCreated,
                               c.PresentOwner
                           }).ToList();
                if (ret.Count == 0)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ReturnObject GetById(int Id)
        {
            try
            {
                var ret = (from c in _db.ClubOwners
                           join e in _db.Clubs on c.ClubId equals e.Id

                           select new
                           {
                               c.Id,
                               c.ClubId,
                               c.OwnershipStartDay,
                               e.ClubName,
                               c.IsDeleted,
                               c.DateCreated,
                               c.PresentOwner
                           }).Where(x => x.Id == Id);
                if (ret == null)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return new ReturnObject { Status = false, StatusMessage = errMsg };
            }
        }
    }
}
