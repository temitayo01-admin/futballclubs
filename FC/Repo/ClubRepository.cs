﻿using FC.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Repo
{
    public interface IClubRepository
    {
        ReturnObject GetAll();
        ReturnObject GetById(int Id);
        ReturnObject GetByCountryId(int cotId);
        ReturnObject Add(ClubModel model);
    }
    public class ClubRepository : IClubRepository
    {
        private TyDbContext _db;
        private string errMsg;
        public ClubRepository(TyDbContext db)
        {
            _db = db;
        }
        public ReturnObject Add(ClubModel model)
        {
            try
            {
                _db.Add(model);
                _db.SaveChanges();
                return new ReturnObject { StatusMessage = "Added successfully", Status = true };
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ReturnObject GetAll()
        {
            try
            {
                var ret = (from c in _db.Clubs
                           join e in _db.ClubCountrys on c.CountryId equals e.Id

                           select new
                           {
                               c.ClubName,
                               c.CountryId,
                               e.Name,
                               c.DateCreated,
                               c.NickName,
                               c.EstablishedOn
                           }).ToList();
                if (ret.Count == 0)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return new ReturnObject { Status = false, StatusMessage = errMsg };
            }
        }

        public ReturnObject GetByCountryId(int cotId)
        {
            try
            {
                var ret = (from c in _db.Clubs
                           join e in _db.ClubCountrys on c.CountryId equals e.Id

                           select new
                           {
                               c.ClubName,
                               c.CountryId,
                               e.Name,
                               c.DateCreated,
                               c.NickName,
                               c.EstablishedOn
                           }).Where(x => x.CountryId == cotId).ToList();
                if (ret.Count == 0)
                {
                    return null;
                }

                return new ReturnObject { Data = ret, Status = true };

            }
            catch (Exception)
            {
                return new ReturnObject { StatusMessage = errMsg, Status = false };

            }
        }

        public ReturnObject GetById(int Id)
        {
            try
            {
                var ret = (from c in _db.Clubs
                           join e in _db.ClubCountrys on c.CountryId equals e.Id

                           select new
                           {
                               c.Id,
                               c.ClubName,
                               c.CountryId,
                               e.Name,
                               c.DateCreated,
                               c.NickName,
                               c.EstablishedOn
                           }).Where(x => x.Id == Id);
                if (ret == null)
                {
                    return new ReturnObject { StatusMessage = errMsg, Status = false };

                }
                return new ReturnObject { Data = ret, Status = true };

            }
            catch (Exception)
            {
                return new ReturnObject { StatusMessage = errMsg, Status = false };

            }
        }
    }
}
