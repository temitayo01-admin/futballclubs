﻿using FC.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Repo
{
    public interface IPlayerRepository
    {
        ReturnObject GetAll();
        ReturnObject GetAllValid();
        ReturnObject GetById(int Id);
        IQueryable<JoinedPlayer> Get(int Id);
        ReturnObject GetByCountryId(int Id);
        ReturnObject GetByClubId(int cId);
        ReturnObject DeleteById(int Id);
        ReturnObject Add(Player model);
    }
    public class PlayerRepository : IPlayerRepository
    {
        private TyDbContext _db;
        private string errMsg;
        public PlayerRepository(TyDbContext db)
        {
            _db = db;
        }
        public ReturnObject Add(Player model)
        {
            try
            {
                _db.Add(model);
                 _db.SaveChanges();

                return new ReturnObject { StatusMessage = "Added successfully", Status = true};
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ReturnObject DeleteById(int Id)
        {
            try
            {
                var reslt = _db.ClubOwners.Find(Id);
                if (reslt == null)
                {
                }
                reslt.IsDeleted = true;
                _db.ClubOwners.Update(reslt);
                _db.SaveChanges();
                return new ReturnObject { StatusMessage = "Player Deleted successfully", Status = true };
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IQueryable<JoinedPlayer> Get(int Id)
        {
            try
            {
                return from c in _db.Players
                       join e in _db.Clubs on c.ClubId equals e.Id
                       join f in _db.ClubCountrys on c.CountryId equals f.Id
                       where c.Id.Equals(Id)
                       select new JoinedPlayer() { Clubs = e, Country = f, Players = c };
                
            }
            catch (Exception)
            {
                return null;
            }
        }
        //public IQueryable<VehicleServiceDTO> GetVehicles()
        //{

        //    return
        //        from v in _context.Vehicles
        //        join si in _context.ServiceIntervals on v.Id equals si.VehicleId into loj
        //        from rs in loj.DefaultIfEmpty()
        //        where
        //            v.Schedule == true
        //            && v.Suspend == false
        //        select new VehicleServiceDTO() { Vehicle = v, Repair = rs };

        //}
        public ReturnObject GetAll()
        {
            try
            {
                var ret = (from c in _db.Players
                           join e in _db.Clubs on c.ClubId equals e.Id
                           join f in _db.ClubCountrys on c.CountryId equals f.Id

                           select new
                           {
                               c.ClubId,
                               c.FullName,
                               e.ClubName,
                               c.IsDeleted,
                               c.DateCreated,
                               c.PlayerNumber,
                               c.CountryId,
                               f.Name,
                               c.Position

                           }).ToList();
                if (ret.Count == 0)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return new ReturnObject { Status = false, StatusMessage = errMsg };
            }
        }

        public ReturnObject GetAllValid()
        {
            try
            {
                var ret = (from c in _db.Players
                           join e in _db.Clubs on c.ClubId equals e.Id
                           join f in _db.ClubCountrys on c.CountryId equals f.Id

                           select new
                           {
                               c.ClubId,
                               c.FullName,
                               e.ClubName,
                               c.IsDeleted,
                               c.DateCreated,
                               c.PlayerNumber,
                               c.CountryId,
                               f.Name,
                               c.Position

                           }).Where(x => x.IsDeleted == false).ToList();
                if (ret.Count == 0)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return new ReturnObject { Status = false, StatusMessage = errMsg };
            }
        }

        public ReturnObject GetByClubId(int cId)
        {
            try
            {
                var ret = (from c in _db.Players
                           join e in _db.Clubs on c.ClubId equals e.Id
                           join f in _db.ClubCountrys on c.CountryId equals f.Id

                           select new
                           {
                               c.ClubId,
                               c.FullName,
                               e.ClubName,
                               c.IsDeleted,
                               c.DateCreated,
                               c.PlayerNumber,
                               c.CountryId,
                               f.Name,
                               c.Position

                           }).Where(x => x.ClubId == cId).ToList();
                if (ret.Count == 0)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return new ReturnObject { Status = false, StatusMessage = errMsg };
            }
        }

        public ReturnObject GetByCountryId(int Id)
        {
            try
            {
                var ret = (from c in _db.Players
                           join e in _db.Clubs on c.ClubId equals e.Id
                           join f in _db.ClubCountrys on c.CountryId equals f.Id

                           select new
                           {
                               c.Id,
                               c.ClubId,
                               c.FullName,
                               e.ClubName,
                               c.IsDeleted,
                               c.DateCreated,
                               c.PlayerNumber,
                               c.CountryId,
                               f.Name,
                               c.Position

                           }).Where(x => x.CountryId == Id).ToList();
                if (ret.Count == 0)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return new ReturnObject { Status = false, StatusMessage = errMsg };
            }
        }

        public ReturnObject GetById(int Id)
        {
            try
            {
                var ret = (from c in _db.Players
                           join e in _db.Clubs on c.ClubId equals e.Id
                           join f in _db.ClubCountrys on c.CountryId equals f.Id

                           select new
                           {
                               c.Id,
                               c.ClubId,
                               c.FullName,
                               e.ClubName,
                               c.IsDeleted,
                               c.DateCreated,
                               c.PlayerNumber,
                               c.CountryId,
                               f.Name,
                               c.Position

                           }).Where(x => x.Id == Id);
                if (ret == null)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return new ReturnObject { Status = false, StatusMessage = errMsg };
            }
        }
    }
}
