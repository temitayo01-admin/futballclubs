﻿using FC.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Repo
{
    public interface IClubCountryRepository
    {
        ReturnObject GetAll();
        ReturnObject Add(ClubCountry obj);
    }
    public class ClubCountryRepository : IClubCountryRepository
    {
        private TyDbContext _db;
        private string errMsg;
        public ClubCountryRepository(TyDbContext db)
        {
            _db = db;
        }
        public ReturnObject Add(ClubCountry obj)
        {
            try
            {
                _db.Add(obj);
                _db.SaveChanges();
                return new ReturnObject { StatusMessage = "Added successfully", Status = true };
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ReturnObject GetAll()
        {
            try
            {
                var ret = _db.ClubCountrys.ToList();
                if (ret.Count == 0)
                {
                    return null;
                }
                return new ReturnObject { Status = true, Data = ret };
            }
            catch (Exception)
            {
                return new ReturnObject { Status = false, StatusMessage = errMsg };
            }
        }
    }
}
