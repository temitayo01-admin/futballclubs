﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.FormModel
{
    public class ClubOwnerFormModel
    {
        public int ClubId { get; set; }
        public string PresentOwner { get; set; }
        public DateTime OwnershipStartDay { get; set; }
    }
}
