﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.FormModel
{
    public class ClubFormModel
    {
        public string ClubName { get; set; }
        public string NickName { get; set; }
        public int CountryId { get; set; }
        public DateTime EstablishedOn { get; set; }
    }
}
