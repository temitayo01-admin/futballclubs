﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.FormModel
{
    public class ClubManagerOwnerFormModel
    {
        public int ClubId { get; set; }
        public string PresentManager { get; set; }
        public DateTime ManagerStartDay { get; set; }
    }
}
