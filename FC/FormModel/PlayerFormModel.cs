﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.FormModel
{
    public class PlayerFormModel
    {
        public int CountryId { get; set; }
        public int ClubId { get; set; }
        //public bool IsDeleted { get; set; }
        public int PlayerNumber { get; set; }
        public string FullName { get; set; }
        public string Position { get; set; }
        public IFormFile PlayerPic { get; set; }
    }
}
