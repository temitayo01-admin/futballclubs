﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FC.Migrations
{
    public partial class AddNickName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ClubModels",
                table: "ClubModels");

            migrationBuilder.RenameTable(
                name: "ClubModels",
                newName: "Clubs");

            migrationBuilder.AddColumn<string>(
                name: "NickName",
                table: "Clubs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Clubs",
                table: "Clubs",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Clubs",
                table: "Clubs");

            migrationBuilder.DropColumn(
                name: "NickName",
                table: "Clubs");

            migrationBuilder.RenameTable(
                name: "Clubs",
                newName: "ClubModels");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClubModels",
                table: "ClubModels",
                column: "Id");
        }
    }
}
