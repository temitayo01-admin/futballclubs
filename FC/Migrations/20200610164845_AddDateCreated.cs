﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FC.Migrations
{
    public partial class AddDateCreated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClubCountryModels");

            migrationBuilder.DropTable(
                name: "PlayerModels");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlayerBiometricModels",
                table: "PlayerBiometricModels");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClubOwnerModels",
                table: "ClubOwnerModels");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClubManagerOwnerModels",
                table: "ClubManagerOwnerModels");

            migrationBuilder.RenameTable(
                name: "PlayerBiometricModels",
                newName: "PlayerBiometrics");

            migrationBuilder.RenameTable(
                name: "ClubOwnerModels",
                newName: "ClubOwners");

            migrationBuilder.RenameTable(
                name: "ClubManagerOwnerModels",
                newName: "ClubManagerOwners");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlayerBiometrics",
                table: "PlayerBiometrics",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClubOwners",
                table: "ClubOwners",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClubManagerOwners",
                table: "ClubManagerOwners",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "ClubCountrys",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClubCountrys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Players",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    ClubId = table.Column<int>(type: "int", nullable: false),
                    PlayerNumber = table.Column<int>(type: "int", nullable: false),
                    FullName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Position = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Players", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClubCountrys");

            migrationBuilder.DropTable(
                name: "Players");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlayerBiometrics",
                table: "PlayerBiometrics");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClubOwners",
                table: "ClubOwners");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClubManagerOwners",
                table: "ClubManagerOwners");

            migrationBuilder.RenameTable(
                name: "PlayerBiometrics",
                newName: "PlayerBiometricModels");

            migrationBuilder.RenameTable(
                name: "ClubOwners",
                newName: "ClubOwnerModels");

            migrationBuilder.RenameTable(
                name: "ClubManagerOwners",
                newName: "ClubManagerOwnerModels");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlayerBiometricModels",
                table: "PlayerBiometricModels",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClubOwnerModels",
                table: "ClubOwnerModels",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClubManagerOwnerModels",
                table: "ClubManagerOwnerModels",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "ClubCountryModels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClubCountryModels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlayerModels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ClubId = table.Column<int>(type: "int", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FullName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PlayerNumber = table.Column<int>(type: "int", nullable: false),
                    Position = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerModels", x => x.Id);
                });
        }
    }
}
