﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Model
{
    public class ReturnObject
    {
        public long Id { get; set; }
        public bool Status { get; set; }
        public string StatusMessage { get; set; }
        public dynamic Data { get; set; }
    }
}
