﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Model
{
    public class ClubManagerOwnerModel
    {
        public int Id { get; set; }
        public int ClubId { get; set; }
        public string PresentManager { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime ManagerStartDay { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
