﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Model
{
    public class ClubOwnerModel
    {
        public int Id { get; set; }
        public int ClubId { get; set; }
        public string PresentOwner { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime OwnershipStartDay { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
