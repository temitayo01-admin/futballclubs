﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Model
{
    public class Player
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public int ClubId { get; set; }
        public bool IsDeleted { get; set; }
        public int PlayerNumber { get; set; }
        public string FullName { get; set; }
        public string Position { get; set; }
        public DateTime DateCreated { get; set; }
        [NotMapped]
        public string Passport { get; set; }
    }
    public class JoinedPlayer
    {
        public Player Players { get; set; }
        public ClubModel Clubs { get; set; }
        public ClubCountry Country { get; set; }

    }
}
