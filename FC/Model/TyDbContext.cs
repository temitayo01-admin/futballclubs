﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Model
{
    public class TyDbContext : DbContext
    {
        public TyDbContext()
        {

        }
        public TyDbContext(DbContextOptions<TyDbContext> options)
            : base(options)
        {
        }

        public DbSet<ClubCountry> ClubCountrys { get; set; }
        public DbSet<ClubManagerOwnerModel> ClubManagerOwners { get; set; }
        public DbSet<ClubModel> Clubs { get; set; }
        public DbSet<ClubOwnerModel> ClubOwners { get; set; }
        public DbSet<PlayerBiometricModel> PlayerBiometrics { get; set; }
        public DbSet<Player> Players { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=.; Initial Catalog=Club; Integrated Security=True;ConnectRetryCount=0");
            }
        }
    }
}
