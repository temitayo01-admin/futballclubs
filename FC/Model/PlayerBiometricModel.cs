﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Model
{
    public class PlayerBiometricModel
    {
        public int Id { get; set; }
        public int PlayerId { get; set; }
        public byte[] PlayerPic { get; set; }
        public string Tag { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
