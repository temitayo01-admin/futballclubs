﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Model
{
    public class ClubModel
    {
        public int Id { get; set; }
        public string ClubName { get; set; }
        public string NickName { get; set; }
        public int CountryId { get; set; }
        public DateTime EstablishedOn { get; set; }
        public DateTime DateCreated { get; set; }
        //public ClubCountry Country { get; set; }
    }
}
