﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FC.Helper
{
    public class GeneralFunction
    {
        public static string BytesToString(byte[] bytes)
        {
            if (bytes != null)
            {
                var e = Convert.ToBase64String(bytes, 0, bytes.Length, Base64FormattingOptions.None);
                var K = "data:image/png;base64," + e;
                return K;
            }
            return null;


        }
    }
}
