﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FC.FormModel;
using FC.Model;
using FC.Repo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClubController : ControllerBase
    {
        private IClubRepository _repo;
        public ClubController(IClubRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        [Route("GetAll")]
        public ReturnObject GetAll()
        {
            return _repo.GetAll();
        }

        [HttpPost]
        [Route("Add")]
        public ReturnObject Add([FromBody] ClubFormModel model)
        {
            var obj = new ClubModel
            {
                CountryId = model.CountryId,
                ClubName = model.ClubName,
                EstablishedOn = model.EstablishedOn.Date,
                NickName = model.NickName,
                DateCreated = DateTime.Now
            };
            return _repo.Add(obj);
        }
        [HttpGet]
        [Route("GetById")]
        public ReturnObject GetById(int Id)
        {
            return _repo.GetById(Id);
        }

        [HttpGet]
        [Route("GetByCotId")]
        public ReturnObject GetByCotId(int Id)
        {
            return _repo.GetByCountryId(Id);
        }
    }
}