﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FC.FormModel;
using FC.Model;
using FC.Repo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClubManagerOwnerController : ControllerBase
    {
        private IClubManagerOwnerRepository _repo;
        public ClubManagerOwnerController(IClubManagerOwnerRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        [Route("GetAll")]
        public ReturnObject GetAll()
        {
            return _repo.GetAll();
        }
        [HttpGet]
        [Route("GetAllValid")]
        public ReturnObject GetAllValid()
        {
            return _repo.GetAllValid();
        }

        [HttpPost]
        [Route("Add")]
        public ReturnObject Add([FromBody] ClubManagerOwnerFormModel model)
        {
            var obj = new ClubManagerOwnerModel
            {
                ClubId = model.ClubId,
                IsDeleted = false,
                PresentManager = model.PresentManager,
                ManagerStartDay = model.ManagerStartDay,
                DateCreated = DateTime.Now
            };
            return _repo.Add(obj);
        }

        [HttpGet]
        [Route("GetById")]
        public ReturnObject GetById(int Id)
        {
            return _repo.GetById(Id);
        }
        [HttpPost]
        [Route("DeleteById")]
        public ReturnObject Delete(int Id)
        {
            return _repo.DeleteById(Id);
        }
        [HttpGet]
        [Route("GetByClubId")]
        public ReturnObject GetByClubId(int Id)
        {
            var ret =  _repo.GetByClubId2(Id);
            return new ReturnObject { Data = ret };
        }
    }
}