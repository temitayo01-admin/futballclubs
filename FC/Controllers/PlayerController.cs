﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FC.FormModel;
using FC.Helper;
using FC.Model;
using FC.Repo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        private IPlayerRepository _repo;
        private readonly IPlayerBiometricRepository _docRepo;
        public static ReturnObject _retObj;
        static PlayerController()
        {
            _retObj = new ReturnObject { Id = 0, Status = false, StatusMessage = "", Data = { } };
        }
        public PlayerController(IPlayerRepository repo, IPlayerBiometricRepository docRepo)
        {
            _repo = repo;
            _docRepo = docRepo;
        }

        [HttpGet]
        [Route("GetAll")]
        public ReturnObject GetAll()
        {
            return _repo.GetAll();
        }
        [HttpGet]
        [Route("GetAllValid")]
        public ReturnObject GetAllValid()
        {
            return _repo.GetAllValid();
        }

        [HttpPost]
        [Route("Add")]
        public ReturnObject Add([FromForm] PlayerFormModel model)
        {
            var obj = new Player
            {
                ClubId = model.ClubId,
                IsDeleted = false,
                CountryId = model.CountryId,
                PlayerNumber = model.PlayerNumber,
                FullName = model.FullName,
                Position = model.Position,
                DateCreated = DateTime.Now
            };

            var ret = _repo.Add(obj);

            int id = obj.Id;
            if (ret.Status)
            {
                var obj2 = new PlayerBiometricModel
                { PlayerId = id, DateCreated = DateTime.Now};
                using (var memoryStream = new MemoryStream())
                {
                    model.PlayerPic.CopyTo(memoryStream);
                    var nuArr = memoryStream.ToArray();
                    obj2.PlayerPic = nuArr;
                }
                obj2.Tag = Path.GetExtension(model.PlayerPic.FileName).ToLowerInvariant();
                var retDoc = _docRepo.AddAsync(obj2);
            }
            return ret;

        }


        [HttpGet]
        [Route("GetById")]
        public ReturnObject GetById(int Id)
        {
            var ret = _repo.Get(Id).ToList();
            if(ret.Count > 0)
            {
                
                var retVal = _docRepo.GetById(Id).ToList();
                if (retVal.Count > 0)
                {
                    ret[0].Players.Passport = GeneralFunction.BytesToString(retVal[0].PlayerPic);
                }

                _retObj.Status = true;
                _retObj.Data = ret;
                return _retObj;
            }
            _retObj.Status = false;
            _retObj.StatusMessage = "No Record To Display";
            return _retObj;
        }
        [HttpPost]
        [Route("DeleteById")]
        public ReturnObject Delete(int Id)
        {
            return _repo.DeleteById(Id);
        }
        [HttpGet]
        [Route("GetByClubId")]
        public ReturnObject GetByClubId(int Id)
        {
            return _repo.DeleteById(Id);
        }
    }
}