﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FC.FormModel;
using FC.Model;
using FC.Repo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClubCountryController : ControllerBase
    {
        private IClubCountryRepository _repo;
        public ClubCountryController(IClubCountryRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        [Route("GetAll")]
        public ReturnObject GetAll()
        {
            return _repo.GetAll();
        }

        [HttpPost]
        [Route("Add")]
        public ReturnObject Add([FromBody] ClubCountryFormModel model)
        {
            var obj = new ClubCountry
            {
                Name = model.Name,
                DateCreated = DateTime.Now
            };
            return _repo.Add(obj);
        }
    }
}