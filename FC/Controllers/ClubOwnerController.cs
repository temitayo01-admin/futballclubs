﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FC.FormModel;
using FC.Model;
using FC.Repo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClubOwnerController : ControllerBase
    {
        private IClubOwnerRepository _repo;
        public ClubOwnerController(IClubOwnerRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        [Route("GetAll")]
        public ReturnObject GetAll()
        {
            return _repo.GetAll();
        }
        [HttpGet]
        [Route("GetAllValid")]
        public ReturnObject GetAllValid()
        {
            return _repo.GetAllValid();
        }

        [HttpPost]
        [Route("Add")]
        public ReturnObject Add([FromBody] ClubOwnerFormModel model)
        {
            var obj = new ClubOwnerModel
            {
                ClubId = model.ClubId,
                IsDeleted = false,
                PresentOwner = model.PresentOwner,
                OwnershipStartDay = model.OwnershipStartDay,
                DateCreated = DateTime.Now
            };
            return _repo.Add(obj);
        }

        [HttpGet]
        [Route("GetById")]
        public ReturnObject GetById(int Id)
        {
            return _repo.GetById(Id);
        }
        [HttpPost]
        [Route("DeleteById")]
        public ReturnObject Delete(int Id)
        {
            return _repo.DeleteById(Id);
        }
        [HttpGet]
        [Route("GetByClubId")]
        public ReturnObject GetByClubId(int Id)
        {
            var ret = _repo.GetByClubId2(Id);
            return new ReturnObject { Data = ret };
        }
    }
}